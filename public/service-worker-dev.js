importScripts('https://storage.googleapis.com/workbox-cdn/releases/4.3.1/workbox-sw.js');
console.log('Hello from service-worker_dev.js !');

if (workbox) {
  console.log(`Yay! Workbox is loaded 🎉`); 
  workbox.precaching.precacheAndRoute([
    '/index.html',
    '/offline.html',
    '/manifest.json'
    ]);

workbox.routing.registerRoute(
    /\.js$/,
    new workbox.strategies.NetworkFirst({
    cacheName: 'js-cache',
    }));

  workbox.routing.registerRoute(
    // Cache CSS files.
    /\.css$/,
    // Use cache but update in the background.
    new workbox.strategies.StaleWhileRevalidate({
      // Use a custom cache name.
      cacheName: 'css-cache',
    })
  );
  
  workbox.routing.registerRoute(
    // Cache local images files.
    /img\/.*\.(?:png|jpg|jpeg|svg|gif)$/,
    // Use the cache if it's available.
    new workbox.strategies.CacheFirst({
      // Use a custom cache name.
      cacheName: 'image-img-cache',
      plugins: [
        new workbox.expiration.Plugin({
          // Cache only 20 images.
          maxEntries: 20,
          // Cache for a maximum of a week.
          maxAgeSeconds: 7 * 24 * 60 * 60,
        })
      ],
    })
  );

  workbox.routing.registerRoute(
    // Cache sockjs files.
    /.*sockjs-node.*/,
    // Use cache but update in the background.
    new workbox.strategies.StaleWhileRevalidate({
      // Use a custom cache name.
      cacheName: 'sockjs-cache',
      plugins: [
      new workbox.cacheableResponse.Plugin({
        statuses: [200, 404]
        //headers: {
          //'Content-Type': 'application/json',
        //},
      })
    ]
    })
  );

  workbox.routing.registerRoute(
    '/index.html',
    new workbox.strategies.NetworkFirst({
    cacheName: 'page-cache',
    plugins: [
      new workbox.expiration.Plugin({
        maxEntries: 50,
      })
    ]
    }));

workbox.routing.registerRoute(/\*.html$/, args => {
  return articleHandler.handle(args)
  .then(response => {
    if (!response) {
      return caches.match('/offline.html');
    } else if (response.status === 404) {
      return caches.match('pages/404.html');
    }
    return response;
  });
});

} else {
  console.log(`Boo! Workbox didn't load 😬`);
}
